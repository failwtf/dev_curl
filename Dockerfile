FROM ubuntu:latest as builder

RUN apt-get update && \
    apt-get install -y git libtool make libcurl4 libssl-dev \
        && rm -rf /var/lib/apt/lists/*

WORKDIR /opt

RUN git clone https://gitlab.com/failwtf/dev_curl.git && \
    cd dev_curl && \
    autoreconf -fi && \
    ./configure --with-openssl --prefix=/curl && \
    make -j $(nproc) && \
    make install

FROM ubuntu:latest

COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
COPY --from=builder /curl /usr/local

RUN ldconfig

CMD ["curl", "-V"]
